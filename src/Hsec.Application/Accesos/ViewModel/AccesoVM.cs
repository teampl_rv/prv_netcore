﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Application.RolAccesos.ViewModel;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.Accesos.ViewModel
{
    public class AccesoVM : IMapFrom<Acceso>
    {
        public AccesoVM()
        {
            RolAccesoSet = new HashSet<RolAccesoVM>();
        }
        public int AccesoID { get; set; }
        public int ModuloID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public Modulo Modulo { get; set; }

        public ICollection<RolAccesoVM> RolAccesoSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Acceso, AccesoVM>()
                .ForMember(x => x.AccesoID, m => m.MapFrom(y => (int)y.AccesoID))
                .ForMember(x => x.ModuloID, m => m.MapFrom(y => (int)y.ModuloID))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion));
        }
    }
}
