﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Application.DTs.ViewModel;
using Hsec.Application.RegistroDTs.ViewModel;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.Usuarios.ViewModel
{
    public class UsuarioVM : IMapFrom<Usuario>
    {
        public UsuarioVM()
        {
            DTSet = new HashSet<DTVM>();
            RegistroDTSet = new HashSet<RegistroDTVM>();
        }

        public int UsuarioID { get; set; }
        public int? RolID { get; set; }
        public int DNI { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Password { get; set; }
        public string Dispositivo { get; set; }

        /// atributos de NAVEGACION

        public ICollection<DTVM> DTSet { get; set; }
        public ICollection<RegistroDTVM> RegistroDTSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Usuario, UsuarioVM>()
                .ForMember(x => x.UsuarioID, m => m.MapFrom(y => (int)y.UsuarioID))
                .ForMember(x => x.DNI, m => m.MapFrom(y => (int)y.DNI))
                .ForMember(x => x.Nombres, m => m.MapFrom(y => y.Nombres))
                .ForMember(x => x.Apellidos, m => m.MapFrom(y => y.Apellidos))
                .ForMember(x => x.Dispositivo, m => m.MapFrom(y => y.Dispositivo))
                .ForMember(x => x.RegistroDTSet, m => m.MapFrom(y => y.RegistroDTSet))
                .ForMember(x => x.DTSet, m => m.MapFrom(y => y.DTSet));

        }
    }
}
