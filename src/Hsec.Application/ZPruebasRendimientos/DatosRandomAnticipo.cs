﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;
using Hsec.Application.Common.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Hsec.Domain.Entities;

namespace Hsec.Application.ZPruebasRendimientos
{
    public class DatosRandomAnticipo : IRequest<Unit>
    {
        public int Rows { get; set; }
        public int DTID { get; set; }

        public class DatosRandomAnticipoHandler : IRequestHandler<DatosRandomAnticipo, Unit>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public DatosRandomAnticipoHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<Unit> Handle(DatosRandomAnticipo request, CancellationToken cancellationToken)
            {
                //Agregando la cantidad 'Rows'  de registros a tabla 'Anticipo'
                var random = new Random();
                var anticipoList = new List<Anticipo>();
                for (int i = 0; i < request.Rows; ++i) 
                {
                    var a = new Anticipo() {
                        DTID = request.DTID,
                        CodigoAnticipo = "ANTI00" + (i+3).ToString(),
                        Descripcion = "",
                        Monto = random.Next(1000)
                    };
                    anticipoList.Add(a);
                }
                await _context.Anticipos.AddRangeAsync(anticipoList);
                await _context.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
