﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Application.Gastos.ViewModel;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.TipoDocumentos.ViewModel
{
    public class TipoDocumentoVM : IMapFrom<TipoDocumento>
    {
        public TipoDocumentoVM()
        {
            GastoSet = new HashSet<GastoVM>();
        }
        public int TipoDocumentoID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<GastoVM> GastoSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TipoDocumento, TipoDocumentoVM>()
                .ForMember(x => x.TipoDocumentoID, m => m.MapFrom(y => (int)y.TipoDocumentoID))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion))
                .ForMember(x => x.GastoSet, m => m.MapFrom(y => y.GastoSet));
        }
    }
}
