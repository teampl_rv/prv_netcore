﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Application.RegistroDTs.ViewModel;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.EstadoDTs.ViewModel
{
    public class EstadoDTVM : IMapFrom<EstadoDT>
    {
        public EstadoDTVM()
        {
            RegistroDTSet = new HashSet<RegistroDTVM>();
        }
        public int EstadoDTID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<RegistroDTVM> RegistroDTSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<EstadoDT, EstadoDTVM>()
                .ForMember(x => x.EstadoDTID, m => m.MapFrom(y => (int)y.EstadoDTID))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion))
                .ForMember(x => x.RegistroDTSet, m => m.MapFrom(y => y.RegistroDTSet));
        }
    }
}
