﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.RegistroDTs.ViewModel
{
    public class RegistroDTVM : IMapFrom<RegistroDT>
    {
        public int RegistroDTID { get; set; }
        public int UsuarioID { get; set; }
        public int DTID { get; set; }
        public int EstadoDTID { get; set; }
        public DateTime? Fecha { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<RegistroDT, RegistroDTVM>()
                .ForMember(x => x.RegistroDTID, m => m.MapFrom(y => (int)y.RegistroDTID))
                .ForMember(x => x.UsuarioID, m => m.MapFrom(y => (int)y.UsuarioID))
                .ForMember(x => x.DTID, m => m.MapFrom(y => (int)y.DTID))
                .ForMember(x => x.EstadoDTID, m => m.MapFrom(y => (int)y.EstadoDTID))
                .ForMember(x => x.Fecha, m => m.MapFrom(y => y.Fecha));
        }
    }
}
