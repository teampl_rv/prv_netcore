﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.Anticipos.ViewModel
{
    public class AnticipoCodMontoVM
    {
        public double Monto { get; set; }
        public string CodigoAnticipo { get; set; }
    }
}
