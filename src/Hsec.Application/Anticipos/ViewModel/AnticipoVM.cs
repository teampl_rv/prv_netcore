﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.Anticipos.ViewModel
{
    public class AnticipoVM : IMapFrom<Anticipo>
    {
        public int AnticipoID { get; set; }
        public int DTID { get; set; }
        public string CodigoAnticipo { get; set; }
        public double Monto { get; set; }
        public DateTime? Fecha { get; set; }
        public string Descripcion { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Anticipo, AnticipoVM>()
                .ForMember(x => x.AnticipoID, m => m.MapFrom(y => (int)y.AnticipoID))
                .ForMember(x => x.DTID, m => m.MapFrom(y => (int)y.DTID))
                .ForMember(x => x.CodigoAnticipo, m => m.MapFrom(y => y.CodigoAnticipo))
                .ForMember(x => x.Monto, m => m.MapFrom(y => y.Monto))
                .ForMember(x => x.Fecha, m => m.MapFrom(y => y.Fecha))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion));

        }
    }
}
