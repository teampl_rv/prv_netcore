﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.Gastos.ViewModel
{
    public class GastoVM : IMapFrom<Gasto>
    {
        public int GastoID { get; set; }
        public int DTID { get; set; }
        public int TipoDocumentoID { get; set; }
        public int EstadoGastoID { get; set; }
        public int TipoGastoID { get; set; }
        public double Monto { get; set; }
        public string RUC { get; set; }
        public string CodigoDocumento { get; set; }
        public DateTime? Fecha { get; set; }
        public string Observaciones { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Gasto, GastoVM>()
                .ForMember(x => x.GastoID, m => m.MapFrom(y => (int)y.GastoID))
                .ForMember(x => x.DTID, m => m.MapFrom(y => (int)y.DTID))
                .ForMember(x => x.TipoDocumentoID, m => m.MapFrom(y => y.TipoDocumentoID))
                .ForMember(x => x.Monto, m => m.MapFrom(y => (double)y.Monto))
                .ForMember(x => x.RUC, m => m.MapFrom(y => y.RUC))
                .ForMember(x => x.CodigoDocumento, m => m.MapFrom(y => y.CodigoDocumento))
                .ForMember(x => x.Fecha, m => m.MapFrom(y => y.Fecha))
                .ForMember(x => x.Observaciones, m => m.MapFrom(y => y.Observaciones));
        }
    }
}
