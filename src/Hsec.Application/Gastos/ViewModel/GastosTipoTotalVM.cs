﻿using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.Gastos.ViewModel
{
    public class GastosTipoTotalVM {

        //suma de todos los Montos de un solo 'TipoGasto'
        public double Monto { get; set; }

        //Descripcion del 'TipoGasto'
        public string Descripcion { get; set; }

        //prioridad del 'TipoGasto'
        public int Prioridad { get; set; }
    }
}
