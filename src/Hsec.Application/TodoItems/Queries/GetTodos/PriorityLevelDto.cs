﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.TodoItems.Queries.GetTodos
{
    public class PriorityLevelDto
    {
        public int Value { get; set; }

        public string Name { get; set; }
    }
}
