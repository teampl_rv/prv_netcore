﻿using Hsec.Application.Common.Mappings;
using System;
using System.Collections.Generic;
using System.Text;
using Hsec.Domain.Entities;
using Hsec.Application.Accesos.ViewModel;
using AutoMapper;
using Hsec.Application.Usuarios.ViewModel;
using Hsec.Application.RolAccesos.ViewModel;

namespace Hsec.Application.Roles.ViewModel
{
    public class RolVM : IMapFrom<Rol>
    {
        public RolVM()
        {
            UsuarioSet = new HashSet<UsuarioVM>();
            RolAccesoSet = new HashSet<RolAccesoVM>();
        }
        public int RolID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<UsuarioVM> UsuarioSet { get; set; }
        public ICollection<RolAccesoVM> RolAccesoSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Rol, RolVM>()
                .ForMember(x => x.RolID, m => m.MapFrom(y => (int)y.RolID))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion))
                .ForMember(x => x.UsuarioSet, m => m.MapFrom(y => y.UsuarioSet))
                .ForMember(x => x.RolAccesoSet, m => m.MapFrom(y => y.RolAccesoSet));
        }
    }

}
