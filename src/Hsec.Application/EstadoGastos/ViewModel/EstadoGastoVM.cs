﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Application.Gastos.ViewModel;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.EstadoGastos.ViewModel
{
    public class EstadoGastoVM : IMapFrom<EstadoGasto>
    {
        public EstadoGastoVM()
        {
            GastoSet = new HashSet<GastoVM>();
        }
        public int EstadoGastoID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<GastoVM> GastoSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<EstadoGasto, EstadoGastoVM>()
                .ForMember(x => x.EstadoGastoID, m => m.MapFrom(y => (int)y.EstadoGastoID))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion))
                .ForMember(x => x.GastoSet, m => m.MapFrom(y => y.GastoSet));
        }
    }
}
