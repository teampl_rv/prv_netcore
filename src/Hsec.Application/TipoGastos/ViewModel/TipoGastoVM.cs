﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Application.Gastos.ViewModel;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.TipoGastos.ViewModel
{
    public class TipoGastoVM : IMapFrom<TipoGasto>
    {
        public TipoGastoVM()
        {
            GastoSet = new HashSet<GastoVM>();
        }

        public int TipoGastoID { get; set; }
        public int Prioridad { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<GastoVM> GastoSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TipoGasto, TipoGastoVM>()
                .ForMember(x => x.TipoGastoID, m => m.MapFrom(y => (int)y.TipoGastoID))
                .ForMember(x => x.Prioridad, m => m.MapFrom(y => (int)y.Prioridad))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion))
                .ForMember(x => x.GastoSet, m => m.MapFrom(y => y.GastoSet));
        }
    }
}
