﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Application.DTs.ViewModel;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.TipoChoferes.ViewModel
{
    public class TipoChoferVM : IMapFrom<TipoChofer>
    {
        public TipoChoferVM()
        {
            DTSet = new HashSet<DTVM>();
        }
        public int TipoChoferID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<DTVM> DTSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<TipoChofer, TipoChoferVM>()
                .ForMember(x => x.TipoChoferID, m => m.MapFrom(y => (int)y.TipoChoferID))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion))
                .ForMember(x => x.DTSet, m => m.MapFrom(y => y.DTSet));
        }
    }
}
