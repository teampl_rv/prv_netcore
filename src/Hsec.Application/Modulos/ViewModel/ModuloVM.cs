﻿using Hsec.Application.Common.Mappings;
using System;
using System.Collections.Generic;
using System.Text;
using Hsec.Domain.Entities;
using Hsec.Application.Accesos.ViewModel;
using AutoMapper;

namespace Hsec.Application.Modulos.ViewModel
{
    public class ModuloVM : IMapFrom<Modulo>
    {
        public ModuloVM()
        {
            AccesoSet = new HashSet<AccesoVM>();
        }
        public int ModuloID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<AccesoVM> AccesoSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Modulo, ModuloVM>()
                .ForMember(x => x.ModuloID, m => m.MapFrom(y => (int)y.ModuloID))
                .ForMember(x => x.AccesoSet, m => m.MapFrom(y => y.AccesoSet))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion));
        }
    }

}
