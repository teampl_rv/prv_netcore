﻿using AutoMapper;
using Hsec.Application.Common.Mappings;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.RolAccesos.ViewModel
{
    public class RolAccesoVM : IMapFrom<RolAcceso>
    {
        public int RolAccesoID { get; set; }
        public int RolID { get; set; }
        public int AccesoID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public Rol Rol { get; set; }
        public Acceso Acceso { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<RolAcceso, RolAccesoVM>()
                .ForMember(x => x.RolAccesoID, m => m.MapFrom(y => (int)y.RolAccesoID))
                .ForMember(x => x.RolID, m => m.MapFrom(y => (int)y.RolID))
                .ForMember(x => x.AccesoID, m => m.MapFrom(y => (int)y.AccesoID))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion));
        }
    }
}
