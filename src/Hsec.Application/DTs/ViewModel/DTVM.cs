﻿using AutoMapper;
using Hsec.Application.Anticipos.ViewModel;
using Hsec.Application.Common.Mappings;
using Hsec.Application.Gastos.ViewModel;
using Hsec.Application.RegistroDTs.ViewModel;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.DTs.ViewModel
{
    public class DTVM : IMapFrom<DT>
    {
        public DTVM()
        {
            GastoSet = new HashSet<GastoVM>();
            AnticipoSet = new HashSet<AnticipoVM>();
            RegistroDTSet = new HashSet<RegistroDTVM>();
        }
        public int DTID { get; set; }
        public int UsuarioID { get; set; }
        public int TipoChoferID { get; set; }
        public string ClaseDT { get; set; }
        public string CodigoDT { get; set; }
        public DateTime? Fecha { get; set; }
        public string Descripcion { get; set; }

        /// Atributos de Navegacion
        public ICollection<GastoVM> GastoSet { get; set; }
        public ICollection<AnticipoVM> AnticipoSet { get; set; }
        public ICollection<RegistroDTVM> RegistroDTSet { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<DT, DTVM>()
                .ForMember(x => x.DTID, m => m.MapFrom(y => (int)y.DTID))
                .ForMember(x => x.UsuarioID, m => m.MapFrom(y => y.UsuarioID))
                .ForMember(x => x.TipoChoferID, m => m.MapFrom(y => y.TipoChoferID))
                .ForMember(x => x.CodigoDT, m => m.MapFrom(y => y.CodigoDT))
                .ForMember(x => x.ClaseDT, m => m.MapFrom(y => y.ClaseDT))
                .ForMember(x => x.Fecha, m => m.MapFrom(y => y.Fecha))
                .ForMember(x => x.Descripcion, m => m.MapFrom(y => y.Descripcion))
                .ForMember(x => x.GastoSet, m => m.MapFrom(y => y.GastoSet))
                .ForMember(x => x.AnticipoSet, m => m.MapFrom(y => y.AnticipoSet))
                .ForMember(x => x.RegistroDTSet, m => m.MapFrom(y => y.RegistroDTSet));

        }
    }
}
