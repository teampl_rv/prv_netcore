﻿using AutoMapper;
using Hsec.Application.Anticipos.ViewModel;
using Hsec.Application.Common.Mappings;
using Hsec.Application.Gastos.ViewModel;
using Hsec.Application.RegistroDTs.ViewModel;
using Hsec.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.DTs.ViewModel
{
    public class DTDetalleVM : IMapFrom<DT>
    {
        public DTDetalleVM()
        {
            AnticiposList = new List<AnticipoCodMontoVM>();
            GastosPorTipoList = new List<GastosTipoTotalVM>();
        }
        public int DTID { get; set; }
        public string CodigoDT { get; set; }
        public string RegistroDTDescripcion { get; set; }
        public string ClaseDT { get; set; }
        public double Saldo { get; set; }
        public string TipoChoferDescripcion { get; set; }
        public double TotalAnticipos { get; set; }
        public List<AnticipoCodMontoVM> AnticiposList { get; set; }
        public double TotalGastos { get; set; }
        public List<GastosTipoTotalVM> GastosPorTipoList { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<DT, DTDetalleVM>()
                .ForMember(x => x.DTID, m => m.MapFrom(y => (int)y.DTID))
                .ForMember(x => x.CodigoDT, m => m.MapFrom(y => y.CodigoDT))
                .ForMember(x => x.ClaseDT, m => m.MapFrom(y => y.ClaseDT));
        }
    }
}
