﻿using AutoMapper;
using Hsec.Application.Anticipos.ViewModel;
using Hsec.Application.Common.Interfaces;
using Hsec.Application.DTs.ViewModel;
using Hsec.Application.Gastos.ViewModel;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Hsec.Application.DTs.Query.GetViewsDT
{
    public class GetDetalleDT : IRequest<DTDetalleVM>
    {
        public int DTID { get; set; }
        public class GetDetalleDTHandler : IRequestHandler<GetDetalleDT, DTDetalleVM>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetDetalleDTHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<DTDetalleVM> Handle(GetDetalleDT request, CancellationToken cancellationToken)
            {
                var dtDetalle = new DTDetalleVM();

                //Obteniendo el registro de DT( OPTIMIZAR LOS INCLUDES )
                var dt = _context.DTs
                    .Include(i => i.AnticipoSet)
                    .Include(i => i.GastoSet)
                        .ThenInclude(i => i.TipoGasto)
                    .FirstOrDefault(f => f.DTID == request.DTID);
                dtDetalle = _mapper.Map<DTDetalleVM>(dt);

                //Obteniendo Tipo de Chofer
                var tipoChofer = _context.TipoChoferes.FirstOrDefault(f => f.TipoChoferID == dt.TipoChoferID);
                dtDetalle.TipoChoferDescripcion = tipoChofer.Descripcion;

                //Obteniendo los Montos de Anticipos y Monto Total de Anticipos
                dtDetalle.TotalAnticipos = 0.0;
                foreach (var a in dt.AnticipoSet)
                {
                    dtDetalle.TotalAnticipos += a.Monto;
                    dtDetalle.AnticiposList.Add(
                        new AnticipoCodMontoVM()
                        {
                            CodigoAnticipo = a.CodigoAnticipo,
                            Monto = a.Monto
                        });
                }

                //Obteniendo el Total de Gastos y Total Gastos por TipoGasto
                dtDetalle.TotalGastos = 0.0;
                var mapGastos = new SortedDictionary<int,  (string,   double,   int)>();
                //                                   id   descripcion  MontoAcumulado  Prioridad
                foreach (var g in dt.GastoSet)
                {
                    dtDetalle.TotalGastos += g.Monto;
                    int id = g.TipoGasto.TipoGastoID;
                    if (!mapGastos.ContainsKey(id)) {
                        mapGastos[id] = (g.TipoGasto.Descripcion, 0, g.TipoGasto.Prioridad);
                    }
                    (string d, double m, int p) = mapGastos[id];
                    mapGastos[id] = (d, m + g.Monto,p);
                }
                foreach (var m in mapGastos)
                {
                    dtDetalle.GastosPorTipoList.Add(
                        new GastosTipoTotalVM
                        {
                            Descripcion = m.Value.Item1,
                            Monto = m.Value.Item2,
                            Prioridad = m.Value.Item3
                        }); 
                }
                dtDetalle.GastosPorTipoList.Sort((x, y) => x.Prioridad.CompareTo(y.Prioridad));

                //Obteniendo el Saldo Actual
                dtDetalle.Saldo = dtDetalle.TotalAnticipos - dtDetalle.TotalGastos;

                //Obteniendo todos 'RegistrosDT'
                var RegistrosDT = _context.RegistroDTs.Where(w => w.DTID == request.DTID).ToList();

                return dtDetalle;
            }
        }
    }
}
