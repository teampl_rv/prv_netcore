﻿using Hsec.Application.DTs.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

using MediatR;
using Hsec.Application.Common.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Hsec.Application.DTs.Query.GetDTByConditions
{
    public class GetDTByCodigoDT : IRequest<DTVM>
    {
        public string CodigoDT { get; set; }

        public class GetDTByCodigoDTHandler : IRequestHandler<GetDTByCodigoDT, DTVM>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetDTByCodigoDTHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<DTVM> Handle(GetDTByCodigoDT request, CancellationToken cancellationToken)
            {
                var dtvm = new DTVM();
                var dT = _context.DTs.Include(i => i.GastoSet).Include(i => i.AnticipoSet)
                    .FirstOrDefault(d => d.CodigoDT == request.CodigoDT);
                dtvm = _mapper.Map<DTVM>(dT);

                return dtvm;
            }
        }
    }
}
