﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Hsec.Application.Common.Interfaces;
using Hsec.Application.DTs.ViewModel;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hsec.Application.DTs.Query.GetDTByConditions
{
    public class GetDTByDTID : IRequest<DTVM>
    {
        public int DTID { get; set; }
        public class GetDTByDTIDHandler : IRequestHandler<GetDTByDTID, DTVM>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetDTByDTIDHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<DTVM> Handle(GetDTByDTID request, CancellationToken cancellationToken)
            {
                var dtvm = new DTVM();
                var dT = _context.DTs.FirstOrDefault(d => d.DTID == request.DTID);
                dtvm = _mapper.Map<DTVM>(dT);

                return dtvm;
            }
        }
    }
}
