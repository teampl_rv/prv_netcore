﻿using System;
using System.Collections.Generic;
using System.Text;

using MediatR;
using Hsec.Application.Common.Interfaces;
using AutoMapper;
using System.Threading.Tasks;
using System.Threading;
using System.Linq;
using Microsoft.EntityFrameworkCore;

using Hsec.Application.Common.General;
using Hsec.Application.DTs.ViewModel;
using AutoMapper.QueryableExtensions;

namespace Hsec.Application.DTs.Query.GetTodos
{
    public class GetTodosDTByUsuarioID : IRequest<GeneralCollection<DTVM>>
    {
        public int UsuarioID { get; set; }

        public class GetTodosDTByUsuarioIDHandler : IRequestHandler<GetTodosDTByUsuarioID, GeneralCollection<DTVM>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetTodosDTByUsuarioIDHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<GeneralCollection<DTVM>> Handle(GetTodosDTByUsuarioID request, CancellationToken cancellationToken)
            {
                var ans = new GeneralCollection<DTVM>();
                var dtsVm = _context.DTs
                    //.Include(i => i.AnticipoSet)
                    //.Include(i => i.GastoSet)
                    .Where(w => w.UsuarioID == request.UsuarioID)
                    .ProjectTo<DTVM>(_mapper.ConfigurationProvider)
                    .ToList();
                ans.Data = dtsVm;
                ans.Count = dtsVm.Count();
                return ans;
            }
        }
    }
}
