﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Hsec.Application.Common.General;
using Hsec.Application.Common.Interfaces;
using Hsec.Application.DTs.ViewModel;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hsec.Application.DTs.Query.GetTodos
{
    public class GetTodosDTs : IRequest<GeneralCollection<DTVM>>
    {
        public class GetTodosDTsHandler : IRequestHandler<GetTodosDTs, GeneralCollection<DTVM>>
        {
            private readonly IApplicationDbContext _context;
            private readonly IMapper _mapper;

            public GetTodosDTsHandler(IApplicationDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<GeneralCollection<DTVM>> Handle(GetTodosDTs request, CancellationToken cancellationToken)
            {
                var vm = new GeneralCollection<DTVM>();
                var dTs = _context.DTs
                    .ProjectTo<DTVM>(_mapper.ConfigurationProvider)
                    .ToList();
                vm.Data = dTs;
                vm.Count = dTs.Count();

                return vm;
            }
        }
    }
}
