﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Hsec.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<TodoItem> TodoItems { get; set; }
        DbSet<Acceso> Accesos { get; set; }
        DbSet<Anticipo> Anticipos { get; set; }
        DbSet<DT> DTs { get; set; }
        DbSet<EstadoDT> EstadoDTs { get; set; }
        DbSet<EstadoGasto> EstadoGastos { get; set; }
        DbSet<Gasto> Gastos { get; set; }
        DbSet<Modulo> Modulos { get; set; }
        DbSet<RegistroDT> RegistroDTs { get; set; }
        DbSet<Domain.Entities.Rol> Roles { get; set; }
        DbSet<RolAcceso> RolAccesos { get; set; }
        DbSet<TipoChofer> TipoChoferes { get; set; }
        DbSet<TipoDocumento> TipoDocumentos { get; set; }
        DbSet<TipoGasto> TipoGastos { get; set; }
        DbSet<Usuario> Usuarios { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
