﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Application.Common.General
{
    public class GeneralCollection<T>
    {
        public IList<T> Data { get; set; }
        public int Count { get; set; }
    }
}
