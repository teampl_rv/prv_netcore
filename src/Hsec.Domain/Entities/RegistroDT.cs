﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class RegistroDT
    {
        public int RegistroDTID { get; set; }
        public int UsuarioID { get; set; }
        public int DTID { get; set; }
        public int EstadoDTID { get; set; }
        public DateTime? Fecha { get; set; }

        /// atributos de NAVEGACION
        public DT DT { get; set; }
        public EstadoDT EstadoDT { get; set; }
        public Usuario Usuario { get; set; }
    }
}
