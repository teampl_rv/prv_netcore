﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class Gasto
    {
        public int GastoID { get; set; }
        public int DTID { get; set; }
        public int TipoDocumentoID { get; set; }
        public int EstadoGastoID { get; set; }
        public int TipoGastoID { get; set; }
        public double Monto { get; set; }
        public string RUC { get; set; }
        public string CodigoDocumento { get; set; }
        public DateTime? Fecha { get; set; }
        public string Observaciones { get; set; }

        /// atributos de NAVEGACION
        public DT DT { get; set; }
        public TipoDocumento TipoDocumento { get; set; }
        public EstadoGasto EstadoGasto { get; set; }
        public TipoGasto TipoGasto { get; set; }

    }
}
