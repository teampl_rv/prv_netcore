﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Hsec.Domain.Entities
{
    public class Anticipo
    {
        public int AnticipoID { get; set; }
        public int DTID { get; set; }
        public string CodigoAnticipo { get; set; }
        public double Monto { get; set; }
        public DateTime? Fecha { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public DT DT { get; set; }
    }
}
