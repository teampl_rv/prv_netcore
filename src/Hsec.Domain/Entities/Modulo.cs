﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class Modulo
    {
        public Modulo()
        {
            AccesoSet = new HashSet<Acceso>();
        }
        public int ModuloID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<Acceso> AccesoSet { get; set; }
    }
}
