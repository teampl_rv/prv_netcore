﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class TipoGasto
    {
        public TipoGasto()
        {
            GastoSet = new HashSet<Gasto>();
        }

        public int TipoGastoID { get; set; }
        public int Prioridad { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<Gasto> GastoSet { get; set; }
    }
}
