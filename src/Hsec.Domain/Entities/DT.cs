﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class DT
    {
        public DT()
        {
            GastoSet = new HashSet<Gasto>();
            AnticipoSet = new HashSet<Anticipo>();
            RegistroDTSet = new HashSet<RegistroDT>();
        }
        public int DTID { get; set; }
        public int UsuarioID{ get; set; }
        public int TipoChoferID { get; set; }
        public string ClaseDT { get; set; }
        public string CodigoDT { get; set; }
        public DateTime? Fecha { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public Usuario Usuario { get; set; }
        public TipoChofer TipoChofer { get; set; }

        public ICollection<Gasto> GastoSet { get; set; }
        public ICollection<Anticipo> AnticipoSet { get; set; }
        public ICollection<RegistroDT> RegistroDTSet { get; set; }

    }
}
