﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class Usuario
    {
        public Usuario() 
        {
            DTSet = new HashSet<DT>();
            RegistroDTSet = new HashSet<RegistroDT>();
        }

        public int UsuarioID {get; set;}
        public int? RolID { get; set; } 
        public int DNI { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string Password { get; set; }
        public string Dispositivo { get; set; }

        /// atributos de NAVEGACION
        public Rol Rol { get; set; }

        public ICollection<DT> DTSet { get; set; }
        public ICollection<RegistroDT> RegistroDTSet { get; set; }
    }
}
