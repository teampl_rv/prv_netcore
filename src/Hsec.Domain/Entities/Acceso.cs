﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class Acceso
    {
        public Acceso()
        {
            RolAccesoSet = new HashSet<RolAcceso>();
        }
        public int AccesoID { get; set; }
        public int ModuloID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public Modulo Modulo { get; set; }

        public ICollection<RolAcceso> RolAccesoSet { get; set; }
    }
}
