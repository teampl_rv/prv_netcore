﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Hsec.Domain.Entities
{
    public class EstadoDT
    {
        public EstadoDT()
        {
            RegistroDTSet = new HashSet<RegistroDT>();
        }
        public int EstadoDTID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<RegistroDT> RegistroDTSet { get; set; }
    }
}
