﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class TipoChofer
    {
        public TipoChofer()
        {
            DTSet = new HashSet<DT>();
        }
        public int TipoChoferID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<DT> DTSet { get; set; }
    }
}
