﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class RolAcceso
    {
        public int RolAccesoID { get; set; }
        public int RolID { get; set; }
        public int AccesoID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public Rol Rol { get; set; }
        public Acceso Acceso { get; set; }
    }
}
