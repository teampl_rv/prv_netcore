﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class Rol
    {
        public Rol()
        {
            UsuarioSet = new HashSet<Usuario>();
            RolAccesoSet = new HashSet<RolAcceso>();
        }
        public int RolID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<Usuario> UsuarioSet { get; set; }
        public ICollection<RolAcceso> RolAccesoSet { get; set; }
    }
}
