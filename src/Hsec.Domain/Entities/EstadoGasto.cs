﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Hsec.Domain.Entities
{
    public class EstadoGasto
    {
        public EstadoGasto()
        {
            GastoSet = new HashSet<Gasto>();
        }
        public int EstadoGastoID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<Gasto> GastoSet { get; set; }
    }
}
