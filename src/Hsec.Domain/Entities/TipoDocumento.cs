﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Domain.Entities
{
    public class TipoDocumento
    {
        public TipoDocumento()
        {
            GastoSet = new HashSet<Gasto>();
        }
        public int TipoDocumentoID { get; set; }
        public string Descripcion { get; set; }

        /// atributos de NAVEGACION
        public ICollection<Gasto> GastoSet { get; set; }
    }
}
