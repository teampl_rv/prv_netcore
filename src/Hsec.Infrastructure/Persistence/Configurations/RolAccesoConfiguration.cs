﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class RolAccesoConfiguration : IEntityTypeConfiguration<RolAcceso>
    {
        public void Configure(EntityTypeBuilder<RolAcceso> builder)
        {
            builder.HasKey(k => k.RolAccesoID)
                .HasName("PK_RolAcceso_RolAccesoID");
            builder.Property(t => t.Descripcion).HasMaxLength(150).IsRequired(false);

            builder.HasOne(o => o.Rol).WithMany(m => m.RolAccesoSet)
                .HasForeignKey(fk => fk.RolID);

            builder.HasOne(o => o.Acceso).WithMany(m => m.RolAccesoSet)
                .HasForeignKey(fk => fk.AccesoID);
        }
    }
}
