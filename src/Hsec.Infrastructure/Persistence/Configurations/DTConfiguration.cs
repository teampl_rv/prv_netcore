﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class DTConfiguration : IEntityTypeConfiguration<DT>
    {
        public void Configure(EntityTypeBuilder<DT> builder)
        {
            builder.HasKey(k => k.DTID)
                .HasName("PK_DT_DTID");
            builder.Property(t => t.Descripcion).HasMaxLength(150).IsRequired(false);
            builder.Property(t => t.CodigoDT).HasMaxLength(150).IsRequired(false);

            builder.HasOne(o => o.Usuario).WithMany(m => m.DTSet)
                .HasForeignKey(fk => fk.UsuarioID);
                //.OnDelete(DeleteBehavior.Cascade); /////?????????????????????? xq :/

            builder.HasOne(o => o.TipoChofer).WithMany(m => m.DTSet)
                .HasForeignKey(fk => fk.TipoChoferID);
        }
    }
}
