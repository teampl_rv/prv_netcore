﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class TipoDocumentoConfiguration : IEntityTypeConfiguration<TipoDocumento>
    {
        public void Configure(EntityTypeBuilder<TipoDocumento> builder)
        {
            builder.HasKey(k => k.TipoDocumentoID)
                .HasName("PK_TipoDocumento_TipoDocumentoID");

            builder.Property(t => t.Descripcion).HasMaxLength(150).IsRequired(false);
        }
    }
}
