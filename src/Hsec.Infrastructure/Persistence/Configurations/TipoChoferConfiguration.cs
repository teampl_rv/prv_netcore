﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class TipoChoferConfiguration : IEntityTypeConfiguration<TipoChofer>
    {
        public void Configure(EntityTypeBuilder<TipoChofer> builder)
        {
            builder.HasKey(k => k.TipoChoferID)
                .HasName("PK_TipoChofer_TipoChoferID");

            builder.Property(t => t.Descripcion).HasMaxLength(150).IsRequired(false);
        }
    }
}
