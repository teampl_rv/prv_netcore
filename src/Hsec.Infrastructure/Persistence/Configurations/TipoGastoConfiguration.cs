﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class TipoGastoConfiguration : IEntityTypeConfiguration<TipoGasto>
    {
        public void Configure(EntityTypeBuilder<TipoGasto> builder)
        {
            builder.HasKey(k => k.TipoGastoID)
                .HasName("PK_TipoGasto_TipoGastoID");

            builder.Property(t => t.Descripcion).HasMaxLength(150).IsRequired(false);
        }
    }
}
