﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class EstadoDTConfiguration : IEntityTypeConfiguration<EstadoDT>
    {
        public void Configure(EntityTypeBuilder<EstadoDT> builder)
        {
            builder.HasKey(k => k.EstadoDTID)
                .HasName("PK_EstadoDT_EstadoDTID");
            builder.Property(t => t.Descripcion).HasMaxLength(150).IsRequired(false);

        }
    }
}
