﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class AccesoConfiguration : IEntityTypeConfiguration<Acceso>
    {
        public void Configure(EntityTypeBuilder<Acceso> builder)
        {
            builder.HasKey(k => k.AccesoID)
                .HasName("PK_Acceso_AccesoID");
            builder.Property(t => t.Descripcion).HasMaxLength(150).IsRequired(false);

            builder.HasOne(o => o.Modulo).WithMany(m => m.AccesoSet)
                .HasForeignKey(fk => fk.ModuloID);
        }
    }
}
