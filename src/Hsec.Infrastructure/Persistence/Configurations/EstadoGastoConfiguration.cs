﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class EstadoGastoConfiguration : IEntityTypeConfiguration<EstadoGasto>
    {
        public void Configure(EntityTypeBuilder<EstadoGasto> builder)
        {
            builder.HasKey(k => k.EstadoGastoID)
                .HasName("PK_EstadoGasto_EstadoGastoID");
            builder.Property(t => t.Descripcion).HasMaxLength(150).IsRequired(false);

        }
    }
}
