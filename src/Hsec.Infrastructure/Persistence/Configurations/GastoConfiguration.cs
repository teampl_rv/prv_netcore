﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class GastoConfiguration : IEntityTypeConfiguration<Gasto>
    {
        public void Configure(EntityTypeBuilder<Gasto> builder)
        {
            builder.HasKey(k => k.GastoID)
                .HasName("PK_Gasto_GastoID");
            builder.Property(t => t.RUC).HasMaxLength(150).IsRequired(false);
            builder.Property(t => t.CodigoDocumento).HasMaxLength(150).IsRequired(false);
            builder.Property(t => t.Observaciones).HasMaxLength(150).IsRequired(false);

            builder.HasOne(o => o.TipoDocumento).WithMany(m => m.GastoSet)
                .HasForeignKey(fk => fk.TipoDocumentoID);

            builder.HasOne(o => o.EstadoGasto).WithMany(m => m.GastoSet)
                .HasForeignKey(fk => fk.EstadoGastoID);

            builder.HasOne(o => o.TipoGasto).WithMany(m => m.GastoSet)
                .HasForeignKey(fk => fk.TipoGastoID);

            builder.HasOne(o => o.DT).WithMany(m => m.GastoSet)
                .HasForeignKey(fk => fk.DTID)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
