﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class UsuarioConfiguration : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.HasKey(k => k.UsuarioID)
                .HasName("PK_Usuario_UsuarioID");

            builder.Property(t => t.Password).HasMaxLength(50).IsRequired(false);
            builder.Property(t => t.Dispositivo).HasMaxLength(50).IsRequired(false);
            builder.Property(t => t.Nombres).HasMaxLength(50).IsRequired(false);
            builder.Property(t => t.Apellidos).HasMaxLength(50).IsRequired(false);

            builder.HasOne(o => o.Rol).WithMany(m => m.UsuarioSet)
                .HasForeignKey(fk => fk.RolID);
        }
    }
}
