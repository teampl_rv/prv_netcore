﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class RegistroDTConfiguration : IEntityTypeConfiguration<RegistroDT>
    {
        public void Configure(EntityTypeBuilder<RegistroDT> builder)
        {
            builder.HasKey(k => k.RegistroDTID)
                .HasName("PK_RegistroDT_RegistroDTID");

            builder.HasOne(o => o.DT).WithMany(m => m.RegistroDTSet)
                .HasForeignKey(fk => fk.DTID).OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(o => o.EstadoDT).WithMany(m => m.RegistroDTSet)
                .HasForeignKey(fk => fk.EstadoDTID);

            builder.HasOne(o => o.Usuario).WithMany(m => m.RegistroDTSet)
                .HasForeignKey(fk => fk.UsuarioID).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
