﻿using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hsec.Infrastructure.Persistence.Configurations
{
    public class AnticipoConfiguration : IEntityTypeConfiguration<Anticipo>
    {
        public void Configure(EntityTypeBuilder<Anticipo> builder)
        {
            builder.HasKey(k => k.AnticipoID)
                .HasName("PK_Anticipo_AnticipoID");
            builder.Property(t => t.Descripcion).HasMaxLength(150).IsRequired(false);

            builder.HasOne(o => o.DT).WithMany(m => m.AnticipoSet)
                .HasForeignKey(fk => fk.DTID)
                .OnDelete(DeleteBehavior.Cascade); /// ????xq :/
        }
    }
}
