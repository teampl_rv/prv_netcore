﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Hsec.Infrastructure.Persistence.Migrations
{
    public partial class EntitiesRaciemsaViaticosApp_V1_Migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EstadoDTs",
                columns: table => new
                {
                    EstadoDTID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstadoDT_EstadoDTID", x => x.EstadoDTID);
                });

            migrationBuilder.CreateTable(
                name: "EstadoGastos",
                columns: table => new
                {
                    EstadoGastoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstadoGasto_EstadoGastoID", x => x.EstadoGastoID);
                });

            migrationBuilder.CreateTable(
                name: "Modulos",
                columns: table => new
                {
                    ModuloID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Modulo_ModuloID", x => x.ModuloID);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    RolID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rol_RolID", x => x.RolID);
                });

            migrationBuilder.CreateTable(
                name: "TipoChoferes",
                columns: table => new
                {
                    TipoChoferID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoChofer_TipoChoferID", x => x.TipoChoferID);
                });

            migrationBuilder.CreateTable(
                name: "TipoDocumentos",
                columns: table => new
                {
                    TipoDocumentoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDocumento_TipoDocumentoID", x => x.TipoDocumentoID);
                });

            migrationBuilder.CreateTable(
                name: "TipoGastos",
                columns: table => new
                {
                    TipoGastoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Prioridad = table.Column<int>(nullable: false),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoGasto_TipoGastoID", x => x.TipoGastoID);
                });

            migrationBuilder.CreateTable(
                name: "Accesos",
                columns: table => new
                {
                    AccesoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModuloID = table.Column<int>(nullable: false),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Acceso_AccesoID", x => x.AccesoID);
                    table.ForeignKey(
                        name: "FK_Accesos_Modulos_ModuloID",
                        column: x => x.ModuloID,
                        principalTable: "Modulos",
                        principalColumn: "ModuloID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    UsuarioID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RolID = table.Column<int>(nullable: false),
                    DNI = table.Column<int>(nullable: false),
                    Nombres = table.Column<string>(maxLength: 50, nullable: true),
                    Apellidos = table.Column<string>(maxLength: 50, nullable: true),
                    Password = table.Column<string>(maxLength: 50, nullable: true),
                    Dispositivo = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuario_UsuarioID", x => x.UsuarioID);
                    table.ForeignKey(
                        name: "FK_Usuarios_Roles_RolID",
                        column: x => x.RolID,
                        principalTable: "Roles",
                        principalColumn: "RolID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RolAccesos",
                columns: table => new
                {
                    RolAccesoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RolID = table.Column<int>(nullable: false),
                    AccesoID = table.Column<int>(nullable: false),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RolAcceso_RolAccesoID", x => x.RolAccesoID);
                    table.ForeignKey(
                        name: "FK_RolAccesos_Accesos_AccesoID",
                        column: x => x.AccesoID,
                        principalTable: "Accesos",
                        principalColumn: "AccesoID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RolAccesos_Roles_RolID",
                        column: x => x.RolID,
                        principalTable: "Roles",
                        principalColumn: "RolID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DTs",
                columns: table => new
                {
                    DTID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UsuarioID = table.Column<int>(nullable: false),
                    TipoChoferID = table.Column<int>(nullable: false),
                    CodigoDT = table.Column<string>(maxLength: 150, nullable: true),
                    Fecha = table.Column<DateTime>(nullable: true),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DT_DTID", x => x.DTID);
                    table.ForeignKey(
                        name: "FK_DTs_TipoChoferes_TipoChoferID",
                        column: x => x.TipoChoferID,
                        principalTable: "TipoChoferes",
                        principalColumn: "TipoChoferID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DTs_Usuarios_UsuarioID",
                        column: x => x.UsuarioID,
                        principalTable: "Usuarios",
                        principalColumn: "UsuarioID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Anticipos",
                columns: table => new
                {
                    AnticipoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DTID = table.Column<int>(nullable: false),
                    CodigoAnticipo = table.Column<string>(nullable: true),
                    monto = table.Column<double>(nullable: false),
                    Fecha = table.Column<DateTime>(nullable: true),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anticipo_AnticipoID", x => x.AnticipoID);
                    table.ForeignKey(
                        name: "FK_Anticipos_DTs_DTID",
                        column: x => x.DTID,
                        principalTable: "DTs",
                        principalColumn: "DTID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Gastos",
                columns: table => new
                {
                    GastoID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DTID = table.Column<int>(nullable: false),
                    TipoDocumentoID = table.Column<int>(nullable: false),
                    EstadoGastoID = table.Column<int>(nullable: false),
                    TipoGastoID = table.Column<int>(nullable: false),
                    Monto = table.Column<double>(nullable: false),
                    RUC = table.Column<string>(maxLength: 150, nullable: true),
                    CodigoDocumento = table.Column<string>(maxLength: 150, nullable: true),
                    Fecha = table.Column<DateTime>(nullable: true),
                    Observaciones = table.Column<string>(maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Gasto_GastoID", x => x.GastoID);
                    table.ForeignKey(
                        name: "FK_Gastos_DTs_DTID",
                        column: x => x.DTID,
                        principalTable: "DTs",
                        principalColumn: "DTID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Gastos_EstadoGastos_EstadoGastoID",
                        column: x => x.EstadoGastoID,
                        principalTable: "EstadoGastos",
                        principalColumn: "EstadoGastoID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Gastos_TipoDocumentos_TipoDocumentoID",
                        column: x => x.TipoDocumentoID,
                        principalTable: "TipoDocumentos",
                        principalColumn: "TipoDocumentoID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Gastos_TipoGastos_TipoGastoID",
                        column: x => x.TipoGastoID,
                        principalTable: "TipoGastos",
                        principalColumn: "TipoGastoID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "RegistroDTs",
                columns: table => new
                {
                    RegistroDTID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UsuarioID = table.Column<int>(nullable: false),
                    DTID = table.Column<int>(nullable: false),
                    EstadoDTID = table.Column<int>(nullable: false),
                    Fecha = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RegistroDT_RegistroDTID", x => x.RegistroDTID);
                    table.ForeignKey(
                        name: "FK_RegistroDTs_DTs_DTID",
                        column: x => x.DTID,
                        principalTable: "DTs",
                        principalColumn: "DTID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RegistroDTs_EstadoDTs_EstadoDTID",
                        column: x => x.EstadoDTID,
                        principalTable: "EstadoDTs",
                        principalColumn: "EstadoDTID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_RegistroDTs_Usuarios_UsuarioID",
                        column: x => x.UsuarioID,
                        principalTable: "Usuarios",
                        principalColumn: "UsuarioID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accesos_ModuloID",
                table: "Accesos",
                column: "ModuloID");

            migrationBuilder.CreateIndex(
                name: "IX_Anticipos_DTID",
                table: "Anticipos",
                column: "DTID");

            migrationBuilder.CreateIndex(
                name: "IX_DTs_TipoChoferID",
                table: "DTs",
                column: "TipoChoferID");

            migrationBuilder.CreateIndex(
                name: "IX_DTs_UsuarioID",
                table: "DTs",
                column: "UsuarioID");

            migrationBuilder.CreateIndex(
                name: "IX_Gastos_DTID",
                table: "Gastos",
                column: "DTID");

            migrationBuilder.CreateIndex(
                name: "IX_Gastos_EstadoGastoID",
                table: "Gastos",
                column: "EstadoGastoID");

            migrationBuilder.CreateIndex(
                name: "IX_Gastos_TipoDocumentoID",
                table: "Gastos",
                column: "TipoDocumentoID");

            migrationBuilder.CreateIndex(
                name: "IX_Gastos_TipoGastoID",
                table: "Gastos",
                column: "TipoGastoID");

            migrationBuilder.CreateIndex(
                name: "IX_RegistroDTs_DTID",
                table: "RegistroDTs",
                column: "DTID");

            migrationBuilder.CreateIndex(
                name: "IX_RegistroDTs_EstadoDTID",
                table: "RegistroDTs",
                column: "EstadoDTID");

            migrationBuilder.CreateIndex(
                name: "IX_RegistroDTs_UsuarioID",
                table: "RegistroDTs",
                column: "UsuarioID");

            migrationBuilder.CreateIndex(
                name: "IX_RolAccesos_AccesoID",
                table: "RolAccesos",
                column: "AccesoID");

            migrationBuilder.CreateIndex(
                name: "IX_RolAccesos_RolID",
                table: "RolAccesos",
                column: "RolID");

            migrationBuilder.CreateIndex(
                name: "IX_Usuarios_RolID",
                table: "Usuarios",
                column: "RolID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Anticipos");

            migrationBuilder.DropTable(
                name: "Gastos");

            migrationBuilder.DropTable(
                name: "RegistroDTs");

            migrationBuilder.DropTable(
                name: "RolAccesos");

            migrationBuilder.DropTable(
                name: "EstadoGastos");

            migrationBuilder.DropTable(
                name: "TipoDocumentos");

            migrationBuilder.DropTable(
                name: "TipoGastos");

            migrationBuilder.DropTable(
                name: "DTs");

            migrationBuilder.DropTable(
                name: "EstadoDTs");

            migrationBuilder.DropTable(
                name: "Accesos");

            migrationBuilder.DropTable(
                name: "TipoChoferes");

            migrationBuilder.DropTable(
                name: "Usuarios");

            migrationBuilder.DropTable(
                name: "Modulos");

            migrationBuilder.DropTable(
                name: "Roles");
        }
    }
}
