﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hsec.Infrastructure.Persistence.Migrations
{
    public partial class EntitiesRaciemsaApp_v7_Migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "monto",
                table: "Anticipos",
                newName: "Monto");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Monto",
                table: "Anticipos",
                newName: "monto");
        }
    }
}
