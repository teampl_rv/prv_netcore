﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Hsec.Infrastructure.Persistence.Migrations
{
    public partial class EntitiesRaciemsaApp_v8_Migration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ClaseDT",
                table: "DTs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ClaseDT",
                table: "DTs");
        }
    }
}
