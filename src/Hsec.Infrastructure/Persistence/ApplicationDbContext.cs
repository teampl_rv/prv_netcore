﻿using Hsec.Application.Common.Interfaces;
using Hsec.Domain.Common;
using Hsec.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace Hsec.Infrastructure.Persistence
{
    public class ApplicationDbContext : DbContext, IApplicationDbContext
    {
        private readonly IDateTime _dateTime;

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IDateTime dateTime)
            : base(options)
        {
            _dateTime = dateTime;
        }

        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<Acceso> Accesos { get; set; }
        public DbSet<Anticipo> Anticipos { get; set; }
        public DbSet<DT> DTs { get; set; }
        public DbSet<EstadoDT> EstadoDTs { get; set; }
        public DbSet<EstadoGasto> EstadoGastos { get; set; }
        public DbSet<Gasto> Gastos { get; set; }
        public DbSet<Modulo> Modulos { get; set; }
        public DbSet<RegistroDT> RegistroDTs { get; set; }
        public DbSet<Rol> Roles { get; set; }
        public DbSet<RolAcceso> RolAccesos { get; set; }
        public DbSet<TipoChofer> TipoChoferes { get; set; }
        public DbSet<TipoDocumento> TipoDocumentos { get; set; }
        public DbSet<TipoGasto> TipoGastos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<AuditableEntity>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreadoPor = "admin";
                        entry.Entity.Creado = _dateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.ModificadoPor = "admin";
                        entry.Entity.Modificado = _dateTime.Now;
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }
    }
}
