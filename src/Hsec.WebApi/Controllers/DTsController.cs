﻿using Hsec.Application.Common.General;
using Hsec.Application.DTs.Query.GetDTByConditions;
using Hsec.Application.DTs.Query.GetTodos;
using Hsec.Application.DTs.Query.GetViewsDT;
using Hsec.Application.DTs.ViewModel;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hsec.WebApi.Controllers
{
    public class DTsController : ApiController
    {
        [HttpPost("GetDetalleDT")]
        public async Task<ActionResult<DTDetalleVM>> PostDetalleDT(GetDetalleDT query) 
        {
            return await Mediator.Send(query);
        }

        [HttpPost]
        public async Task<ActionResult<GeneralCollection<DTVM>>> Post(GetTodosDTs command)
        {
            return await Mediator.Send(command);
        }

        [HttpPost("GetDTByID")]
        public async Task<ActionResult<DTVM>> PostByID(GetDTByDTID command)
        {
            return await Mediator.Send(command);
        }

        [HttpPost("GetDTByCodigoDT")]
        public async Task<ActionResult<DTVM>> PostByID(GetDTByCodigoDT command)
        {
            return await Mediator.Send(command);
        }

        [HttpPost("GetTodosDTByUsuarioID")]
        public async Task<ActionResult<GeneralCollection<DTVM>>> PostTodosDTByUsuarioID(GetTodosDTByUsuarioID command)
        {
            return await Mediator.Send(command);
        }
    }
}
