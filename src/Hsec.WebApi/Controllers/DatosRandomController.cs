﻿
using Hsec.Application.ZPruebasRendimientos;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hsec.WebApi.Controllers
{
    public class DatosRandomController : ApiController
    {
        [HttpPost("DatosRandomAnticipo")]
        public async Task<ActionResult<Unit>> PostRandomAnticipo(DatosRandomAnticipo d)
        {
            return await Mediator.Send(d);
        }
    }
}
    